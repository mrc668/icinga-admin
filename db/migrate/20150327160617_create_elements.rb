class CreateElements < ActiveRecord::Migration
  def change
    create_table :elements do |t|
      t.string :name
      t.string :alias
      t.string :hostGroup
      t.string :conf

      t.timestamps null: false
    end
    add_index :elements, :name, unique: true
  end
end
