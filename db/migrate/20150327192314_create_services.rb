class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :name
      t.string :period
      t.string :description
      t.string :check_command
      t.string :url
      t.boolean :active
      t.string :serviceGroup

      t.timestamps null: false
    end
    add_index :services, :name, unique: true
  end
end
