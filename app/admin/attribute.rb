ActiveAdmin.register Attribute do
  permit_params :name, :description
end
