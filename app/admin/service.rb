ActiveAdmin.register Service do
  permit_params :name, :period, :description, :check_command, :url, :active, :serviceGroup
end
