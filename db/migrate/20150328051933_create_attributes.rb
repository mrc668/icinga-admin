class CreateAttributes < ActiveRecord::Migration
  def change
    create_table :attributes do |t|
      t.string :name
      t.string :description

      t.timestamps null: false
    end
    add_index :attributes, :name, unique: true
  end
end
